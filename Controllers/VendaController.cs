using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        private static List<Pedido> listaPedidos = new List<Pedido>();

        [HttpPost("registar")]
        public ActionResult<Pedido> CriarPedido(Pedido pedido)
        {
            if (listaPedidos.Count == 0)
                pedido.Id = 1;
            else
                pedido.Id = listaPedidos.Max(x => x.Id) + 1;

            if (pedido.Itens.Count == 0)
                return BadRequest("Erro: A lista de pedidos deve haver pelo menos 1 (um) item.");
            pedido.Status = StatusVenda.AguardandoPagamento;
            listaPedidos.Add(pedido);
            return Ok(pedido);
        }

        [HttpGet("exibirTodos")]
        public ActionResult<List<Pedido>> MostrarTodos()
        {
            if (listaPedidos.Count == 0)
                return Ok("A lista está vazia.");
            return Ok(listaPedidos);
        }

        [HttpGet("buscar/{id}")]
        public ActionResult<Pedido> MostrarUm(int id)
        {
            var buscaPorId = listaPedidos.FirstOrDefault(x => x.Id == id);
            if (buscaPorId == null)
                return BadRequest(new { Erro = "Não foi encontrado o pedido." });
            return Ok(buscaPorId);
        }

        [HttpPut("alterarStatus/{id}")]
        public ActionResult<Pedido> Atualizar(int id, StatusVenda novoStatus)
        {
            var buscaPorId = listaPedidos.FirstOrDefault(x => x.Id == id);
            if (buscaPorId == null)
                return BadRequest(new { Erro = "pedido não foi encontrado." });

            if (novoStatus == StatusVenda.Cancelado)
            {
                buscaPorId.Status = StatusVenda.Cancelado;
                return Ok($"o pedido {buscaPorId} foi cancelado.");
            }
            else if (buscaPorId.Status == StatusVenda.Cancelado || buscaPorId.Status == StatusVenda.Entregue)
            {
                return BadRequest(new { Erro = "O pedido já foi cancelado ou entregue, não foi possível alterar" });
            }
            else if (novoStatus == buscaPorId.Status + 1)
            {
                buscaPorId.Status++;
                return Ok($"o Status do item {buscaPorId} foi atualizado para {novoStatus}");
            }
            return BadRequest(new { Erro = "Houve um erro, por favor refaça a operação corretamente." });
        }

    }
}