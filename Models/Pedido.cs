namespace tech_test_payment_api
{
    public class Pedido
    {
        public int Id { get; set; }
        public Vendedor Vendedor { get; set; } = new Vendedor();
        public List<string> Itens { get; set; } = new List<string>();
        public DateTime HoraVenda { get; set; } 
        public StatusVenda Status { get; set; }
        
    }
}